using System.Collections.Generic;

namespace book.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string Names { get; set; }
        public string Surnames { get; set; }
        public IList<Book> Books { get; set; } = new List<Book>();
    }
}