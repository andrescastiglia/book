using System.Collections.Generic;

namespace book.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<Book> Books { get; set; } = new List<Book>();
    }
}