using Microsoft.EntityFrameworkCore;

namespace book.Models 
{
    public class BookContext: DbContext 
    {
        public BookContext(DbContextOptions<BookContext> options)
            : base(options)
        {
        }
            
        public DbSet<Book> Book { get; set; }
        public DbSet<Author> Author { get; set; }
        public DbSet<Category> Category { get; set; }
    }

}