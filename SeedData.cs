using System;
using book.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace book
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var options = serviceProvider.GetRequiredService<DbContextOptions<BookContext>>();

            using (var dbContext = new BookContext(options))
            {
                /* 
                var any = dbContext.Book.AnyAsync().GetAwaiter().GetResult();

                if (any)
                {
                    return;
                }
                */

                var novell = new Category() { Name = "Novell" };
                var fiction = new Category() { Name = "Fiction" };

                var borges = new Author(){ Names = "Jorge Luis", Surnames = "Borges" };
                var cortazar = new Author() { Names = "Julio", Surnames = "Cortazar" };

                var elaleph = new Book() { Name = "El Aleph", Author = borges, Category = fiction };
                var rayuela = new Book() { Name = "Rayuela", Author = cortazar, Category = novell };

                dbContext.AddRange(elaleph, rayuela);
                dbContext.SaveChanges();
            }
        }
    }
}