﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using book.Models;
using Microsoft.EntityFrameworkCore;

namespace book.Controllers
{
    public class AuthorController : Controller
    {
        private BookContext _bookContext;

        public AuthorController(BookContext bookContext) {
            _bookContext = bookContext;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _bookContext.Author.ToListAsync());
        }

        public async Task<IActionResult> Search(int id)
        {
            return View(await _bookContext.Author.FirstOrDefaultAsync(x => x.Id == id) );
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
