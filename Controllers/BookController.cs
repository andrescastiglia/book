﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using book.Models;
using Microsoft.EntityFrameworkCore;

namespace book.Controllers
{
    public class BookController : Controller
    {
        private BookContext _bookContext;

        public BookController(BookContext bookContext) {
            _bookContext = bookContext;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _bookContext.Book
                .Include( x => x.Category )
                .Include( x => x.Author )
                .ToListAsync());
        }

        public async Task<IActionResult> Search(int id)
        {
            return View(await _bookContext.Book.FirstOrDefaultAsync(x => x.Id == id));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
